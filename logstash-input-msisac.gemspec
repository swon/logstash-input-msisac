Gem::Specification.new do |s|

  s.name            = 'logstash-input-msisac'
  s.version         = '1.0.0'
  s.licenses        = ['Apache License (2.0)']
  s.summary         = "Poll the MS-ISAC feed"
  s.description     = "This gem is a Logstash plugin required to be installed on top of the Logstash core pipeline using $LS_HOME/bin/logstash-plugin install gemname. This gem is not a stand-alone program"
  s.authors         = ["Teamworx Security"]
  s.email           = 'stephen@teamworxsecurity.com'
  s.homepage        = "http://www.elastic.co/guide/en/logstash/current/index.html"
  s.require_paths   = ["lib"]

  # Files
  s.files = Dir["lib/**/*","*.gemspec","*.md","Gemfile","LICENSE","vendor/jar-dependencies/**/*.jar", "vendor/jar-dependencies/**/*.rb", "VERSION", "docs/**/*"]

  # Special flag to let us know this is actually a logstash plugin
  s.metadata = { "logstash_plugin" => "true", "logstash_group" => "input" }

  # Gem dependencies
  s.add_runtime_dependency "logstash-core-plugin-api", ">= 1.60", "<= 2.99"
  s.add_runtime_dependency 'logstash-codec-plain'
  s.add_runtime_dependency 'addressable'
  s.add_runtime_dependency 'faraday'
  s.add_runtime_dependency 'stud', "~> 0.0.22"
  s.add_runtime_dependency 'nokogiri'

  s.add_development_dependency 'logstash-devutils'
end
