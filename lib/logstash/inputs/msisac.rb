# encoding: utf-8
require "logstash/inputs/base"
require "logstash/namespace"
require "socket" # for Socket.gethostname
require "stud/interval"
require "faraday"
require "nokogiri"

# Run command line tools and capture the whole output as an event.
#
# Notes:
#
# * The `@source` of this event will be the command run.
# * The `@message` of this event will be the entire stdout of the command
#   as one event.
#
class LogStash::Inputs::Msisac < LogStash::Inputs::Base

  config_name 'msisac'

  default :codec, 'plain'

  # MSISAC feed URL
  config :url, :validate => :string, :required => true

  # Interval to run the command. Value is in seconds.
  config :interval, :validate => :number, :required => true

  public
  def register
    @logger.info("Registering MSISAC Input", :url => @url, :interval => @interval)
  end # def register

  public
  def run(queue)
    @run_thread = Thread.current
    until stop?
      start = Time.now
      @logger.info? && @logger.info('Polling MSISAC', :url => @url)

      # Pull down the MSISAC feed using FTW so we can make use of future cache functions
      response = Faraday.get @url
      handle_response(response, queue)

      duration = Time.now - start
      @logger.info? && @logger.info("Command completed", :command => @command,
                                    :duration => duration)

      # Sleep for the remainder of the interval, or 0 if the duration ran
      # longer than the interval.
      sleeptime = [0, @interval - duration].max
      if sleeptime == 0
        @logger.warn("Execution ran longer than the interval. Skipping sleep.",
                     :command => @command, :duration => duration,
                     :interval => @interval)
      else
        Stud.stoppable_sleep(sleeptime) { stop? }
      end
    end # loop
  end

  def handle_response(response, queue)
    body = response.body
    begin
      doc = Nokogiri::XML.parse(body)
      doc.xpath('//item').each do |item|
        handle_rss_response(queue, item)
      end
    rescue => e
      @logger.error('Unknown error while parsing the feed', :url => url, :exception => e)
    end
  end

  def stop
    Stud.stop!(@run_thread) if @run_thread
  end

  private
  def handle_rss_response(queue, item)
    @codec.decode(item.xpath('description').text) do |event|
      event.set('published', item.xpath('dc:date').text)
      event.set('title', item.xpath('title').text)
      event.set('link', item.xpath('link').text)
      event.set("Feed", @url)
      decorate(event)
      queue << event
    end
  end
end # class LogStash::Inputs::Exec
